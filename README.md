## Movilia
**Library to connect to Movilia API using PHP**


### Example
```php
<?php
use Movilia\Sms;

require "vendor/autoload.php";
error_reporting(E_ALL);

define('MOVILIA_USER', 'USER');
define('MOVILIA_PASSWORD', 'MD5PASSWORD');

try {
$sms = new Sms(MOVILIA_USER, MOVILIA_PASSWORD);
$sms->addSender('xxxx'); 
$sms->addCountryCode(00); // 34 <- Spain
$sms->addMobile(0000000000); 
$sms->addMessage('Test message');
$sms->setResponseFormat('json');
$sms->setDeliveryReport(false);
$response = $sms->send();
var_dump($response);
}
catch (Exception $exception)
{
echo " Code :: ".$exception->getCode()." Error :: ".$exception->getMessage()."\n";
}
?>
```

