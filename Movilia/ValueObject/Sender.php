<?php

namespace Movilia\ValueObject;


class Sender
{
    private $sender;

    public function __construct($sender)
    {
        $this->sender = $sender;
        if (strlen($sender) >11){throw new \Exception("Sender is most long to authorized"); }
        return $this->sender;
    }

    public function __invoke()
    {
        return $this->sender;
    }
}
