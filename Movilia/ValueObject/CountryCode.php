<?php

namespace Movilia\ValueObject;

use megastruktur\PhoneCountryCodes;

class CountryCode
{
    protected $countrycode;
    protected PhoneCountryCodes $phoneCountryCodes;
    protected $phoneCodeList;
    public function __construct(int $countryCode)
    {
        $this->countrycode = str_replace('+','', $countryCode);

        $this->phoneCountryCodes = new PhoneCountryCodes();
        $this->phoneCodeList = $this->phoneCountryCodes->getCodesFullList();
        $this->isValidCountryCode($this->countrycode);
        if(!$this->existCountryCode($this->countrycode)){
            throw new \Exception('Country Code not exist in list of countries');
        }
        return $this->countrycode;
    }

    protected function isValidCountryCode(int $countryCode)
    {
        if (empty($countryCode)){
            throw new \Exception('Country Code is empty');
        }

        if (!is_int($countryCode)){
            throw new \Exception('Country Code is not number');
        }
        return true;
    }

    protected function existCountryCode($countryCode){
        $result = false;
        $countryCode = str_replace('+','', $countryCode);
        foreach ($this->phoneCodeList as $list){
            if ($list['dial_code'] == "+".$countryCode){
                $result =  true;
            }
        }
        return $result;
    }


    public function __invoke()
    {
      return $this->countrycode;
    }
}
