<?php

namespace Movilia\ValueObject;


class Message
{
    private String $message;

    public function __construct($message){

     if (empty($message)){
         throw new \Exception('Message to send empty!', '001');
     }
        $this->message = $message;

    }

    public function __invoke()
    {
        return urlencode($this->message);
    }

}
