<?php

namespace Movilia\ValueObject;


class Mobile
{
    private $mobile;
    public function __construct($mobile)
    {
        $this->mobile = $mobile;
        return $this->mobile;
    }

    public function __invoke()
    {
        return $this->mobile;
    }
}
