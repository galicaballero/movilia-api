<?php

namespace Movilia\Connector;

class WalletGet
{
    public function __construct(protected string $username, protected string $password){
    }

    public function __invoke(){
        $url = 'http://movilia.com/getscore';

        $complete_url = $url.'?login='.$this->username.'&password='.$this->password;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $complete_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

}