<?php
namespace Movilia\Connector;


class SmsSend
{
    public function __construct(protected string $username, protected string $password)
    {

    }

    public function submit( $countryCode,  $mobile,  $message,  $sender, $delay = false, bool $deliveryReport = false){
        $url = 'http://movilia.com/http2sms';

        $complete_url = $url.'?login='.$this->username.'&password='.$this->password
            .'&countrycode='.$countryCode.'&phone='.$mobile.'&message='.$message.'&sender='.$sender;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $complete_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}
