<?php

namespace Movilia;

use Movilia\Connector\SmsSend;
use Movilia\Connector\WalletGet;
use Movilia\ValueObject\CountryCode;
use Movilia\ValueObject\Message;
use Movilia\ValueObject\Mobile;
use Movilia\ValueObject\Sender;

final class Sms
{
    private string $user;
    private string $password;

    private $deliveryReportStatus = false;

    protected CountryCode $countryCode;
    protected WalletGet $walletGet;
    protected SmsSend $smsSend;
    protected $mobile;
    protected $sender;
    protected $message;
    protected $responseFormat = 'json';


    public function __construct($user, $password)
    {
        $this->user = $user;
        $this->password = $password;
        $this->smsSend = new SmsSend($this->user, $this->password);
        $this->walletGet = new WalletGet($this->user, $this->password);
    }

    public function addCountryCode($countryCode)
    {
        if (empty($countryCode)) {
            throw new \Exception('Country Code is empty', 103);
        }
        $this->countryCode = new CountryCode($countryCode);
        return $this;
    }

    public function addMobile($mobile)
    {
        $this->mobile[] = new Mobile($mobile);
        return $this;
    }

    public function addSender($sender)
    {
        $this->sender = new Sender($sender);
        return $this;
    }

    public function addMessage($message)
    {
        $this->message = new Message($message);
        return $this;
    }

    public function send(\DateTime $dateDelay = null)
    {
        $delay = false;

        if (isset($this->sender)) {
            $sender = ($this->sender)();
        } else {
            $sender = "";
        }

        $deliveryReport = $this->deliveryReportStatus;
        if (!isset($this->countryCode)) {
            throw new \Exception("Country Code is undefined", 101);
        }
        if (!isset($this->mobile[0])) {
            throw new \Exception("Mobile number is undefined", 201);
        }
        if (!isset($this->message)) {
            throw new \Exception("Message is undefined", 301);
        }

        $output = $this->smsSend->submit(($this->countryCode)(), ($this->mobile[0])(),
            ($this->message)(), $sender, $delay, $deliveryReport);


        $json_output = [];
        if ($this->responseFormat == 'json') {
            $xml = simplexml_load_string($output);
            $json_output[] = $xml;
            return json_encode($json_output);
        } elseif ($this->responseFormat == 'xml') {
            return $output;
        }
        return true;
    }

    public function setDeliveryReport(bool $status)
    {
        $this->deliveryReportStatus = $status;
    }

    public function setResponseFormat($format = 'xml')
    {
        $this->responseFormat = $format;
    }

    public function getWallet()
    {
        $output =  ($this->walletGet)();
        if ($this->responseFormat == 'json'){
            $xml = simplexml_load_string($output);
            $json_output[] = $xml;
            return json_encode($json_output);
        } elseif ($this->responseFormat == 'xml') {
            return $output;
        }
        return true;
    }

}
